package com.example.masallo_android_app

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class MainActivity2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        this.setTitle("Health Information")

        val HaveSymptons = findViewById<TextView>(R.id.HaveSymptons)
        val HaveConfirmed = findViewById<TextView>(R.id.HaveConfirmed)

        findViewById<Button>(R.id.Next).setOnClickListener {

            val qwe1 = intent.getStringExtra("Fullname").toString()
            val qwe2 = intent.getStringExtra("MobileNumber").toString()
            val qwe3 = intent.getStringExtra("City").toString()

            Toast.makeText(applicationContext, qwe1 + " hayssssssssssss", Toast.LENGTH_LONG).show()


            val intent = Intent(this, MainActivity3::class.java)
            intent.putExtra("Fullname", qwe1)
            intent.putExtra("MobileNumber", qwe2)
            intent.putExtra("City", qwe3)
            intent.putExtra("HaveSymptons", HaveSymptons.text.toString())
            intent.putExtra("HaveConfirmed", HaveConfirmed.text.toString())
            startActivity(intent)
        }
    }
}