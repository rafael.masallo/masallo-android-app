package com.example.masallo_android_app

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity3 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main3)

        this.setTitle("Confirmation")

        var recyclerView = findViewById<RecyclerView>(R.id.ListOutput)
        val data = arrayListOf<String>();
        data.add(intent.getStringExtra("Fullname").toString())
        data.add(intent.getStringExtra("MobileNumber").toString())
        data.add(intent.getStringExtra("City").toString())
        data.add(intent.getStringExtra("HaveSymptons").toString())
        data.add(intent.getStringExtra("HaveConfirmed").toString())
        Toast.makeText(applicationContext, intent.getStringExtra("Fullname").toString(), Toast.LENGTH_LONG).show()

        val adapter = Adapter(this, data)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter

        findViewById<Button>(R.id.Done).setOnClickListener {
            val intent = Intent(this, MainActivity4::class.java)
            startActivity(intent)
        }


    }
}